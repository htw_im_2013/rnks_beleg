/* multicast_client.c
 * This sample demonstrates a Windows multicast client that works with either
 * IPv4 or IPv6, depending on the multicast address given.
 * Requires Windows XP+/Use MSVC and platform SDK to compile.
 * Troubleshoot: Make sure you have the IPv6 stack installed by running
 *     >ipv6 install
 *
 * Usage:
 *     multicast_client multicastip port
 *
 * Examples:
 *     >multicast_client 224.0.22.1 9210
 *     >multicast_client ff15::1 2001
 *
 * Written by tmouse, July 2005
 * http://cboard.cprogramming.com/showthread.php?t=67469
 */
 
#include <stdio.h>      /* for printf() and fprintf() */
#include <winsock2.h>   /* for socket(), connect(), sendto(), and recvfrom() */
#include <ws2tcpip.h>   /* for ip_mreq */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <time.h>       /* for timestamps */

#if defined(_MSC_VER)
#pragma comment(lib, "ws2_32.lib")
#endif


void DieWithError(char* errorMessage) {
    fprintf(stderr, "%s\n", errorMessage);
    exit(EXIT_FAILURE);
}


int main(int argc, char* argv[]) {
    SOCKET     sock;                     /* Socket */
    WSADATA    wsaData;                  /* For WSAStartup */
    char*      multicastIP;              /* Arg: IP Multicast Address */
    char*      multicastPort;            /* Arg: Port */
    ADDRINFO*  multicastAddr;            /* Multicast Address */
    ADDRINFO*  localAddr;                /* Local address to bind to */
    ADDRINFO   hints          = { 0 };   /* Hints for name lookup */

    int trueValue = 1;
    char* responseString = "UNICAST response from client!";
    static struct sockaddr_in6 remoteAddr;
    int remoteAddrSize = sizeof(struct sockaddr_in6);

    if ( WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) { DieWithError("WSAStartup() failed"); }

    if ( argc != 3 ) {
        fprintf(stderr,"Usage: %s <Multicast IP> <Multicast Port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    multicastIP   = argv[1];      /* First arg:  Multicast IP address */
    multicastPort = argv[2];      /* Second arg: Multicast port */

    /* Resolve the multicast group address */
    hints.ai_family = AF_INET6;
    hints.ai_flags  = AI_NUMERICHOST;
    if ( getaddrinfo(multicastIP, NULL, &hints, &multicastAddr) != 0 ) {DieWithError("getaddrinfo() failed");}

    printf("\n\n**** Client started ****\nUsing %s\n", multicastAddr->ai_family == PF_INET6 ? "IPv6" : "IPv4");

    /* Get a local address with the same family (IPv4 or IPv6) as our multicast group */
    hints.ai_family   = multicastAddr->ai_family;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags    = AI_PASSIVE; /* Return an address we can bind to */
    if ( getaddrinfo(NULL, multicastPort, &hints, &localAddr) != 0 ) {DieWithError("getaddrinfo() failed");}

    /* Create socket for receiving datagrams */
    if ( (sock = socket(localAddr->ai_family, localAddr->ai_socktype, 0)) == INVALID_SOCKET ) {DieWithError("socket() failed");}

    /* Reuse multicast address and multicast port for serveral clients */
    if ( setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&trueValue, sizeof (trueValue)) != 0 ) {DieWithError("setsockopt() failed");}

    /* Bind to the multicast port */
    if ( bind(sock, localAddr->ai_addr, localAddr->ai_addrlen) != 0 ) {DieWithError("bind() failed");}

    /* Multicast address join structure */
    struct ipv6_mreq multicastRequest;

    /* Specify the multicast group */
    memcpy(&multicastRequest.ipv6mr_multiaddr, &((struct sockaddr_in6*)(multicastAddr->ai_addr))->sin6_addr, sizeof(multicastRequest.ipv6mr_multiaddr));

    /* Accept multicast from any interface */
    multicastRequest.ipv6mr_interface = 0;

    /* Join the multicast address */
    if ( setsockopt(sock, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, (char*) &multicastRequest, sizeof(multicastRequest)) != 0 ) {DieWithError("setsockopt() failed");}

    freeaddrinfo(localAddr);
    freeaddrinfo(multicastAddr);

    for (;;) {
        time_t timer;
        char   recvString[500];      /* Buffer for received string */
        int    recvStringLen;        /* Length of received string */

        /* Receive a single datagram from the server */
        if ( (recvStringLen = recvfrom(sock, recvString, sizeof(recvString) - 1, 0, (struct sockaddr *)&remoteAddr, &remoteAddrSize)) < 0 ) {DieWithError("recvfrom() failed");}

        recvString[recvStringLen] = '\0';

        /* Print the received string */
        time(&timer);
        printf("Time Received: %.*s : %s\n", strlen(ctime(&timer)) - 1, ctime(&timer), recvString);

        if (strcmp("server1", recvString) == 0) {
            printf("\nSEND UNICAST RESPONSE to server1\n");
			sendto(sock, responseString, strnlen(responseString, 1024), 0, (struct sockaddr *)&remoteAddr, sizeof(remoteAddr));
        }
    }

    /* NOT REACHED */
    closesocket(sock);
    exit(EXIT_SUCCESS);
}