struct timeouts{
	unsigned long seq_nr; 
	unsigned long timer;
	struct timeouts *next;
};


struct timeouts* add_timer(struct timeouts *list, int timer_val, unsigned long seq_nr);
struct timeouts* del_timer(struct timeouts *list, unsigned long seq_nr);
int decrement_timer(struct timeouts *list);
long seq_nr;
