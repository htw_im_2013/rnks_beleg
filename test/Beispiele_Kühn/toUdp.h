#define DEFAULT_SERVER		"FF11::10"	// will use the loopback interface
#define DEFAULT_FAMILY		AF_INET6	// accept IPv6
#define DEFAULT_SOCKETYPE	SOCK_DGRAM	// UDP | SOCK_STREAM (TCP)
#define DEFAULT_PORT		"50000"		// 50000 (test port)
#define DEFAULT_WINDOW		"4"			// Default WindowSize
#define UNKNOWN_NAME "<unknown>"
