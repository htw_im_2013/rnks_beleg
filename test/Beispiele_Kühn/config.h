#define P_MESSAGE_1 "\nHow to use.\n"

#define P_MESSAGE_2 "\n%s [-a adress] [-p port]\n\n"
#define P_MESSAGE_3 " address\tIP address on which to bind.  (default; unspecified address)\n"
#define P_MESSAGE_4 " port\t\tPort on which to bin.  (default: %s)\n"
#define P_MESSAGE_5 "Hit Ctrl-C tp terminate\n"

#define P_MESSAGE_6 "\n%s [-s server] [-p port] [-f file] [-w window_size]\n\n"
#define P_MESSAGE_7 " server\tName or IP address on which to connect. (default: %s)\n"
#define P_MESSAGE_8 " port\t\tPrt on which to connect. (default: %s)"
#define P_MESSAGE_9 " file\t\tFile which to recieve. (default: empty)"
#define P_MESSAGE_10 " window_size\t\twindow size of Reliable MC Protocol (maximum: 10)."
