// gcc -Wall -o byte-order byte-order.c

#include <stdio.h>
#include <netinet/in.h>

int main(int argc, char const *argv[]) {
	short a, b;

	a = 384;
	b = htons(a);

	if (a == b)	{
		printf("big endian system\n");
	} else {
		printf("little endian system\n");
	}

	printf("Hex: a = %04hx \t b = %04hx\n", a, b);

	return 0;
}