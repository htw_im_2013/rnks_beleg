// gcc -Wall -o filetest filetest.c

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>

int main(int argc, char const *argv[]) {
	int fileDesc;
	char buffer[256];

	fileDesc = open("a.txt", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

	strcpy(buffer, "Hello world!");

	write(fileDesc, buffer, strlen(buffer));

	bzero(buffer, sizeof(buffer));

	lseek(fileDesc, 0, SEEK_SET);

	read(fileDesc, buffer, sizeof(buffer));

	printf("%s\n", buffer);

	close(fileDesc);

	return 0;
}