/*selbstgebaut!*/

#include "serverSy.h"			// SAP to our protocol
#include "data.h"
#include "toUdp.h"			
#include <dos.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <winsock2.h>

void createfile(struct request *reqPtr, int *init_file_create)
{
	FILE *fp;

	if ((fp = fopen(reqPtr->fname, "w+t")) == 0)  // create file
	{
		printf("\nFehler: konnte Datei %s nicht oeffnen\a\n", reqPtr->fname); exit (0);
	}
	(*init_file_create) = 1;			// indicate that file was created
	fclose(fp);
}

int writeFile(char lol[][256], struct request *reqPtr)
{
	FILE *fp;
	int i, j;

	if ((fp = fopen(reqPtr->fname, "a+t")) == 0)		// open file
	{
		printf("\nFehler: konnte Datei %s nicht oeffnen\a\n", reqPtr->fname); exit(0);
	}
	for (j = 0; (j < 100); j++)			// print buffer into file
	{
		for (i = 0; (i < 256) /*&& (lol[j][i] != '\0')*/; i++)
		{
			if (lol[j][i] != '\0')		
				fputc(lol[j][i], fp);
			else						//stop if end of file ie reached
			{
				fputc(lol[j][i], fp);
				fclose(fp);
				return 1;
			}
		}
	}
	fclose(fp);
	return 0;
}

struct answer *answreturn(struct request *reqPtr, int *sqnr_counter, int *window_size, int *drop_pack_sqnr)
{
	static struct answer   answ;

	if ((reqPtr->FlNr) == 0)
	{
		answ.AnswType = AnswErr;
		answ.ErrNo = 4;
		return(&answ);
	}
	switch (reqPtr->ReqType) 
	{
		case ReqHello:
			answ.AnswType = AnswHello;
			answ.FlNr = reqPtr->FlNr;
			answ.SeNo = reqPtr->SeNr;
			(*window_size) = reqPtr->FlNr;
			return(&answ);
			break;
		case ReqData:
			if ((reqPtr->SeNr != 0))
			{
				if ((reqPtr->SeNr>0) && (reqPtr->name>0) && ((reqPtr->SeNr == (*sqnr_counter)) || (reqPtr->SeNr == (*drop_pack_sqnr))))	// Sqn Nr > 0
				{
					answ.AnswType = AnswOk;
					answ.FlNr = reqPtr->FlNr;
					answ.SeNo = reqPtr->SeNr;
					(*sqnr_counter)++;
					if (reqPtr->SeNr == (*drop_pack_sqnr))
						(*sqnr_counter)--;
					return(&answ);
				}
				else										// Sqn Nr < 0
				{
					answ.AnswType = AnswErr;
					answ.ErrNo = 3;							// Illegal Sqn Number
					return(&answ);
				}
			}
			else											// Sqn Nr = 0
			{
				answ.AnswType = AnswWarn;
				answ.ErrNo = 1;								// Sequence Nr not available
				return(&answ);
			}
		case ReqClose:
			if (reqPtr->SeNr != 0)
			{ 
				if ((reqPtr->SeNr > 0) /*&& (reqPtr->name > 0)*/ && ((reqPtr->SeNr == (*sqnr_counter)) || (reqPtr->SeNr == (*drop_pack_sqnr))))	// Sqn Nr > 0
				{
					answ.AnswType = AnswClose;
					answ.FlNr = reqPtr->FlNr;
					answ.SeNo = reqPtr->SeNr;
					(*sqnr_counter)++;
					if (reqPtr->SeNr == (*drop_pack_sqnr))
						(*sqnr_counter)--;
					return(&answ);
				}
				else										// Sqn Nr < 0
				{
					answ.AnswType = AnswErr;
					answ.ErrNo = 3;							// Illegal Sqn Number
					return(&answ);
				}
			}
			else											// Sqn Nr = 0
			{
				answ.AnswType = AnswWarn;
				answ.ErrNo = 1;								// Sequence Nr not available
				return(&answ);
			}
		default:
			answ.AnswType = AnswErr;
			answ.ErrNo = 4;									// Illegal Request
			return(&answ);
	}
	answ.AnswType = AnswErr;
	answ.ErrNo = 5;											// Server Internal Error
	return (&answ);
}

int main(int argc, char *argv[])
{
	// init_file_create=first-time-receiving-data->create-new-file-switch | sqnr_counter=counter for normal received data-packages
	// window_size=window size(not used yet) | drop_pack_sqnr=dropped-package-number list
	// c=for for loop | v=for for loop
	// drop=package-drop-indicator-switch
	int init_file_create = 0, sqnr_counter = 1, window_size = 1, drop_pack_sqnr, c, v, drop = 0;
	struct request *reqPtr;		// received request goes here
	struct answer  *answPtr;	// answer to send goes here
	char lol[300][256];			// buffer for received messages (to get them in right order at the end)

	printf("Server active and waiting... (^C exits server) \n");
	initServer();				// initialize server
	for (;;)
	{
		reqPtr = getRequest();	// receive the request
		if (reqPtr->ReqType == ReqData)		// if the request contained data store them in the buffer
		{
			c = (reqPtr->SeNr)-1;
			for (v = 0; v < 256; v++)
			{
				if (lol[c][v] != '\0')				// store until end of file
					lol[c][v] = reqPtr->name[v];	// put the '\0'
				else
					break;
			}
		}
		printf("SeqNr: %d | Type: %c\n", reqPtr->SeNr, reqPtr->ReqType);
		drop = 0;		// reset the package-drop-indicator-switch
		if(init_file_create == 0)		// if its the first data-package create a new file
			createfile(reqPtr, &init_file_create);
		answPtr = answreturn(reqPtr, &sqnr_counter, &window_size, &drop_pack_sqnr); // select the right answer for the request and check if correct SeqNr
		//if (w%((a)) == 0)
		sendAnswer(answPtr, &drop_pack_sqnr, &drop);		// send the answer
		if ((reqPtr->ReqType == ReqClose) && (drop == 0))	// if closing is required and the last package wasnt dropped (could be the close package -> would cause too early close of server)
			break;											// leave the loop ...
	}
	writeFile(lol, reqPtr);										// write the data buffer into the file ...
	exitServer();												// and shut down the server
}