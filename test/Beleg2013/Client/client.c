// only some parts ... of the clientApp

#include "clientSy.h"			// SAP to our protocol
#include "data.h"
#include "toUdp.h"
#include "config.h"
#include <dos.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <winsock2.h>

#include "timer.h"

void Usage(char *ProgName)		// How to use program
{
	fprintf(stderr, P_MESSAGE_l);
	fprintf(stderr, P_MESSAGE_6, ProgName);
	fprintf(stderr, P_MESSAGE_7, (DEFAULT_SERVER == NULL) ? "loopback address" : DEFAULT_SERVER);
	fprintf(stderr, P_MESSAGE_8, DEFAULT_PORT);
	fprintf(stderr, P_MESSAGE_9);
	exit(1);
}

int printAnswer(struct answer *answPtr) // print received answer
{
	switch (answPtr->AnswType)
	{
		case AnswHello:
			printf("Answer Hello");
			break;
		case AnswOk:
			printf("Answer Ok : Packet acknowledged No: %u ", answPtr->SeNo);
			break;
		case AnswClose:
			printf("Answer Close");
			break;
		case AnswWarn:
			printf("Answer Warning: %s", errorTable[answPtr->ErrNo]);
			break;
		case AnswErr:
			printf("Answer Error: %s", errorTable[answPtr->ErrNo]);
			break;
		default:
			printf("Illegal Answer");
			break;
	};							/* switch */
	puts("\n");
	return answPtr->AnswType;
}

int fileselector(struct request *reqptr, char *file, int *fpos, int *ende)
{
	FILE *fp;
	int i;

	if (reqptr->ReqType == ReqData)
	{
		if ((fp = fopen(file, "rt")) == 0)		// open file
		{
			printf("\nFehler: konnte Datei %s nicht oeffnen\a\n", file); return 1;
		}
		fseek(fp, *fpos, SEEK_SET);				// get to the right spot in the file
		for (i = 0; i < PufferSize; i++)
		{
			if ((reqptr->name[i] = fgetc(fp)) == EOF) // if the end of the file is reached
			{
				reqptr->name[i] = '\0';					// put an '\0' ...
				if (reqptr->name[0] == '\0')
				{
					(*ende) = 1;							// and indicate the end (for later use in sendData function)
					reqptr->ReqType = ReqClose;
					i++;
				}
				break;									// stop reading from file
			}
		}
		reqptr->FlNr = i;						// set data-package file-size
		*fpos = ftell(fp);						// save current file position
		fclose(fp);
	}
	return 0;
}

int main(int argc, char *argv[])
{
	// sqnr_counter=counter for normal sent data-packages | fail=message-send-fail-switch
	// ende=indicator for reaching end of file | fpos=last fp-position in file
	int sqnr_counter = 1, fpos = 0, fail = 0, ende = 0;
	struct request request, temp;					// buffer for not sended messages
	struct answer *answerptr;							// received answer is stored here
	struct timeouts *listptr = NULL;					// timeoutlist
	long i;
	char *Server = DEFAULT_SERVER;
	char *Filename = "";
	char *Port = DEFAULT_PORT;	
	char *Window_size = DEFAULT_WINDOW;		// Default Window Size -> prog argument

	if (argc > 1)						// Parameter ueberpruefen
	{
		for (i = 1; i < argc; i++)
		{
			if (((argv[i][0] == '-') || (argv[i][0] == '/')) && (argv[i][1] != 0) && (argv[i][2] == 0))
			{
				switch (tolower(argv[i][1]))
				{
					case 'a':			// Server Adresse
						if (argv[i + 1])
						{
							if (argv[i + 1][0] != '-')
							{
								Server = argv[++i];
								break;
							}
						}
						Usage(argv[0]);
						break;
					case 'p':			// Server Port
						if (argv[i + 1])
						{
							if (argv[i + 1][0] != '-')
							{
								Port = argv[++i];
								break;
							}
						}
						Usage(argv[0]);
						break;
					case 'f':			// File Name
						if (argv[i + 1])
						{
							if (argv[i + 1][0] != '-')
							{
								Filename = argv[++i];
								break;
							}
						}
						Usage(argv[0]);
						break;
					case 'w':			// Window Size
						if (argv[i + 1])
						{
							if (argv[i + 1][0] != '-')
							{
								Window_size = argv[++i];
								
								if (atoi(Window_size) > 10)
									Usage(argv[0]);

								break;
							}
						}
						Usage(argv[0]);
						break;
					default:
						Usage(argv[0]);
						break;
				}
			}
			else
				Usage(argv[0]);
		}
	}
	initClient(Server);								// initialize client

		if (Filename  == "")
	{
		printf ("Kein Dateiname angegeben, sende leeres Packet\n");
		request.FlNr = 1;
		strcpy(request.fname, "empty_file.txt");
		request.name[0] = '\0';
		request.ReqType = ReqClose;
		request.SeNr = 1;
		listptr = sendData(&request, listptr, &sqnr_counter, &fail, &temp, &ende);
		answerptr = recvanswer();
		printAnswer(answerptr);
		if (answerptr->AnswType == AnswClose)	// if reveived answer is close, close the client
		{
			exitClient();
			ende = 1;
		}
	}
	if (ende != 1)
	{
		strcpy(request.fname, Filename);				// set filename
		request.ReqType = ReqHello;						// first packet is hello packet
		if (request.ReqType == ReqHello)
			if (Window_size != 1)						// because of getting errors .... :((
				request.FlNr = atoi(Window_size);
			else
				request.FlNr = (int)Window_size;
		request.SeNr = sqnr_counter;								// set SeNr for hello packet
		request.name[0] = '\0';							// its an empty packet
		listptr = sendData(&request, listptr, &sqnr_counter, &fail, &temp, &ende); // send hello packet
		answerptr = recvanswer();						// receive answer for hello packet
		printAnswer(answerptr);							// print answer for hello packet
		if (Filename != "")								// filename is program argument
			request.ReqType = ReqData;

		for (;;)
		{
			request.SeNr = sqnr_counter;							// set SeNr for data-packages
			if (fail == 0)
					fileselector(&request, Filename, &fpos, &ende);		// select the correct part of the file to send
			listptr = sendData(&request, listptr, &sqnr_counter, &fail, &temp, &ende);	// send data-package
			//if ((r%atoi(Window_size)) == 0)
			answerptr = recvanswer();
			if (fail == 0)								// if sending doenst fail receive answer and print it
			{
				printAnswer(answerptr);
				if (answerptr->AnswType == AnswClose)	// if reveived answer is close, close the client
					break;
			}
		}
		exitClient();		// shut down the client
	}
}