/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ctype.h>

#include "../lib/data.h"
#include "../lib/config.h"
#include "serverUDP.h"
#include "../lib/toUdp.h"


static struct sockaddr_in6 unicastAddr;
ADDRINFO* multicastAddr;
int unicastAddrSize = sizeof(struct sockaddr_in6);
SOCKET cSocket;


void exitServer() {
	closesocket(cSocket);
	if (WSACleanup() == SOCKET_ERROR) {exitWithError("WSACleanup() failed");}
}


void initServer(char* multicastIP, char* multicastPort) {
	WSADATA wsaData;
	ADDRINFO hints = {0};
	struct ipv6_mreq multicastRequest;
	ADDRINFO*  localAddr;
	int trueValue = 1;
	
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {exitWithError("WSAStartup() failed");}

	/* Resolve the multicast group address */
	hints.ai_family = AF_INET6;
	hints.ai_flags  = AI_NUMERICHOST;
	if (getaddrinfo(multicastIP, NULL, &hints, &multicastAddr) != 0) {exitWithError("getaddrinfo() failed");}

	printf("\n\n**** Server started ****\nUsing %s\n", multicastAddr->ai_family == PF_INET6 ? "IPv6" : "IPv4");

	/* Get a local address with the same family (IPv4 or IPv6) as our multicast group */
	hints.ai_family   = multicastAddr->ai_family;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags    = AI_PASSIVE;
	if (getaddrinfo(NULL, multicastPort, &hints, &localAddr) != 0) {exitWithError("getaddrinfo() failed");}

	/* Create socket for receiving datagrams */
	if ((cSocket = socket(localAddr->ai_family, localAddr->ai_socktype, 0)) == INVALID_SOCKET) {exitWithError("socket() failed");}
	
	/* Reuse multicast address and multicast port for serveral clients */
	if (setsockopt(cSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&trueValue, sizeof (trueValue)) != 0) {exitWithError("setsockopt() failed");}

	/* Bind to the multicast port */
	if (bind(cSocket, localAddr->ai_addr, localAddr->ai_addrlen) != 0) {exitWithError("bind() failed");}

	/* Specify the multicast group */
	memcpy(&multicastRequest.ipv6mr_multiaddr, &((struct sockaddr_in6*)(multicastAddr->ai_addr))->sin6_addr, sizeof(multicastRequest.ipv6mr_multiaddr));

	/* Accept multicast from any interface */
	multicastRequest.ipv6mr_interface = 0;

	/* Join the multicast address */
	if (setsockopt(cSocket, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, (char*) &multicastRequest, sizeof(multicastRequest)) != 0) {exitWithError("setsockopt() failed");}

	freeaddrinfo(localAddr);
	freeaddrinfo(multicastAddr);
}


struct frame* receiveFromClient() {
	int receivedBytes;						
	struct frame* frameIn = (struct frame*)malloc(sizeof(struct frame));

	receivedBytes = recvfrom(cSocket, (char *)frameIn, sizeof(*frameIn), 0, (struct sockaddr*) &unicastAddr, &unicastAddrSize);

	if (receivedBytes == SOCKET_ERROR) {
		exitServer();
		exitWithError("recvfrom() failed");
	} else if (receivedBytes == 0) {
		exitServer();
		exitWithError("Client closed connection");
	}

	return frameIn;
}


void sendToClient(struct frame* frameOut) {
	if (sendto(cSocket, (const char*)frameOut, sizeof(*frameOut), 0, (struct sockaddr *)&unicastAddr, unicastAddrSize) == SOCKET_ERROR) {
		exitServer();
		exitWithError("sendto() failed");
	}
}
