/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include "../lib/data.h"

/**
 * [checkConsistency description]
 * @param  buffer       [description]
 * @param  CurrentSeqNr [description]
 * @return              [description]
 */
struct frame* checkConsistency(char **buffer, int CurrentSeqNr);

/**
 * [fillBuffer description]
 * @param frame  [description]
 * @param buffer [description]
 */
void fillBuffer(struct frame* frame,char** buffer);

/**
 * [main description]
 * @param  argc [description]
 * @param  argv [description]
 * @return      [description]
 */
int main(int argc, char* argv[]);
