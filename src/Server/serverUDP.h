/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


/**
 * initialize server with socket and join multicast group
 * @param multicastIP   IP of multicast group
 * @param multicastPort port to bind on
 */
void initServer(char* multicastIP, char* multicastPort);


/**
 * get frame from client
 * @return frame-struct sent from client
 */
struct frame* receiveFromClient();


/**
 * send frame to client
 * @param frameOut frame-struct to send to client
 */
void sendToClient(struct frame* frameOut);


/**
 * close socket connection
 */
void exitServer();
