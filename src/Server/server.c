/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <dos.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <winsock2.h>

#include "serverUDP.h"
#include "../lib/data.h"
#include "../lib/toUdp.h"
#include "../lib/config.h"
#include "../lib/file.h"


struct frame* checkConsistency(char** buffer, int currentSeqNr) {
	int i;
	struct frame* nack;

	for (i = 0; i < currentSeqNr; i++) {
		if (buffer[i] == NULL) {
			nack = (struct frame*)malloc(sizeof(struct frame));
			nack->type  = RES_NACK;
			nack->seqNr = i;
			return nack;
		}
	}
	
	return NULL;
}


void fillBuffer(struct frame* frame, char** buffer) {
	buffer[frame->seqNr] = (char*)malloc(sizeof(char)*BufferSize);
	strcpy(buffer[frame->seqNr], frame->data);
}


int main(int argc, char* argv[]) {
	struct frame *frameIn;
	struct frame *frameOut = (struct frame*)malloc(sizeof(struct frame));
	
	char** buffer;
	char filename[100];	
	char server[100]; 	
	char port[10]; 	
	char windowSize[10];

	int errorRate 		= -1;
	int helloReceived 	= 0;
	int NackCounter     = 0;
	int seqNrLength;
	int i;
	int maxSeqNr;

	/************ INITIALIZE SERVER ************/
	strcpy(filename, "test.txt");
	strcpy(server, DEFAULT_SERVER);
	strcpy(port, DEFAULT_PORT);	
	strcpy(windowSize, DEFAULT_WINDOW);
	if (verifyArguments(argc, argv, server, filename, port, windowSize, &errorRate) == 0) {
		initServer(server, port);
	} else {
		exitWithError("argument validation failed");
	}

	maxSeqNr = (2*atoi(windowSize))-1;

	while (1) {
		frameIn = receiveFromClient();

		switch (frameIn->type){
			case REQ_HELLO:
				printf("received package of type HELLO\n");
				helloReceived = 1;
				strcpy(filename, frameIn->data);
				seqNrLength = frameIn->seqNr;
				buffer = (char**)malloc(sizeof(char*)*seqNrLength);
				for (i = 0; i < seqNrLength; i++) {
					buffer[i] = NULL;
				}
				frameOut->type = RES_HELLO;
				sendToClient(frameOut);
				break;
			case REQ_DATA:
				if (helloReceived) {
					NackCounter++;
					if  ((errorRate > 0) && ((NackCounter % errorRate) == 0)) {continue;}
					printf("received package of type DATA\n\t--> seqNr: %d\n",frameIn->seqNr % maxSeqNr);
					frameOut = checkConsistency(buffer, frameIn->seqNr);
					if (frameOut != NULL) {
						printf("\t\t--> NACK\n");
						fillBuffer(frameIn,buffer);
						sendToClient(frameOut);	
					} else {
						fillBuffer(frameIn, buffer);
					}
				}
				break;
			case REQ_CLOSE:
				if (helloReceived) {
					printf("received package of type CLOSE\n");
					frameOut = checkConsistency(buffer, seqNrLength);
					if (frameOut != NULL) {
						printf("\t\t--> NACK\n");
						sendToClient(frameOut);
					} else {
						frameOut = (struct frame*)malloc(sizeof(struct frame));
						frameOut->type = RES_CLOSE;
						sendToClient(frameOut);
						writeFile(filename, buffer, seqNrLength);
						exitServer();
						exit(0);
					}
				}
				break;
			default:
				break;
		}	
	}

	return 0;
}
