/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


struct fileContent {
	int seqNr;
	int length;
	char* data;
	struct fileContent* next;
};


/**
 * load file into list of data packages
 * @param  file        filename
 * @param  seqNrLength number of packages to send
 * @return             list of data packages
 */
struct fileContent* loadFile(char* file, int* seqNrLength);


/**
 * write file to disk
 * @param  filename    
 * @param  buffer      file content
 * @param  packageSize
 * @return             0: error | 1: ok
 */
int writeFile(char* filename, char** buffer, int packageSize);
