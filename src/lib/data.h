/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#define TIMEOUT_INT	300
#define TIMEOUT		3
#define MAX_WINDOW	10
#define MAX_SEQNR	2*MAX_WINDOW-1
#define MAX_BUFFER	2*MAX_WINDOW
#define REQ_HELLO	'H'
#define REQ_DATA	'D'
#define REQ_CLOSE	'C'
#define RES_HELLO	'H'
#define RES_NACK	'N'
#define RES_CLOSE	'C'
#define RES_ERR		0xFF
#define BufferSize	256

extern char *netError[];

struct frame {
	unsigned char	type;
	unsigned int	seqNr;
	char			data[BufferSize];
	unsigned int    dataLength;
};
