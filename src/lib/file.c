/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "file.h"
#include "../lib/data.h"

extern char *fileError[];


struct fileContent* loadFile(char* file, int* seqNrLength) {
	int i = 0;
	FILE* fp;
	char* buffer = (char*)malloc(sizeof(char)*BufferSize);
	struct fileContent* anchor=NULL, *help, *newElement;

	*seqNrLength = 0;

	if ((fp = fopen(file, "rt")) == 0) {return NULL;}

	while (fgets(buffer, BufferSize, fp) != 0) {
		newElement 			= (struct fileContent*) malloc(sizeof(struct fileContent));
		newElement->data 	= (char*)malloc(sizeof(char)*BufferSize);
		newElement->length	= strlen(buffer);
		newElement->next	= NULL;
		newElement->seqNr 	= (*seqNrLength)++;
		strcpy(newElement->data, buffer);

		if (anchor == NULL) {
			anchor 			= newElement;
			anchor->next	= NULL;
		} else {
			help = anchor;
			while (help->next != NULL) {
				help = help->next;
			}
			help->next 	= newElement;
		}
		free(buffer);
		buffer = (char*)malloc(sizeof(char)*BufferSize);
	}

	return anchor;
}


int writeFile(char* filename, char** buffer, int packageSize) {
	FILE *fp;
	int i,j;

	if ((fp = fopen(filename, "w+t")) == 0) {
		printf("Fehler: Datei %s konnte nicht geschrieben werden\n",filename );
		return 0;
	}

	for (i = 0; i < packageSize; i++) {
		for (j = 0; j < BufferSize; j++) {
			if (buffer[i][j] == '\0') {
				break;
			} else {
				fputc(buffer[i][j], fp);
			}
		}
	}

	fclose(fp);

	return 1;
}
