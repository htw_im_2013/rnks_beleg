/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>  
#include <string.h>

#include "timer.h"


struct timer* add_timer(struct timer *timers, unsigned int seq_nr) {	
	
	struct timer* anchor;

	if (timers == NULL){
		timers = (struct timer*)malloc(sizeof(struct timer));
		timers->next = NULL;
		timers->seq_nr = seq_nr;
		timers->timer = 3;
		anchor = timers;
	} else {
		anchor = timers;
		while (1) {
			if (timers->next == NULL) {
				timers->next = (struct timer*)malloc(sizeof(struct timer));
				timers->next->seq_nr = seq_nr;
				timers->next->next = NULL;
				timers->next->timer = 3;
				break;
			}
			timers = timers->next;
		}
	}

	return anchor;

}


struct timer* del_timer(struct timer* timers, unsigned int seq_nr) {
	struct timer *anchor = timers;
	struct timer *backup = NULL;
	while (timers != NULL) {
		if (timers->seq_nr == seq_nr) {
			if (backup == NULL){
				return timers->next;					
			} else {		
				backup->next = timers->next;
				return anchor;
			}
		}
		backup = timers;
		timers = timers->next;
	}
}


struct timer* decrement_timers(struct timer* timers) {
	struct timer* anchor = timers;
	int first = 1;
	 while (timers != NULL) {
		 timers->timer--;
		 if (timers->timer == 0) {
			 timers = del_timer(timers, timers->seq_nr);
			 if (first) {
				 anchor = timers;
			 }
		 } else {
			timers = timers->next;
			first  = 0;
		 }
	 }
	return anchor;
 }
 