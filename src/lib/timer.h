/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


struct timer {
	unsigned int seq_nr;
	unsigned int timer;
	struct timer* next;
};


/**
 * add a timer to timer list
 * @param  timers list of timer
 * @param  seq_nr frame sequence number to bind timer to
 * @return        anchor of timer list
 */
struct timer* add_timer(struct timer *timers, unsigned int seq_nr);


/**
 * remove first timer from timer list
 * @param  timers list of timer
 * @param  seq_nr frame sequence number to bind timer to
 * @return        anchor of timer list
 */
struct timer* del_timer(struct timer *timers, unsigned long seq_nr);


/**
 * decrement timeout value
 * @param  timers list of timer
 * @return        anchor of timer list
 */
struct timer* decrement_timers(struct timer *timers);
