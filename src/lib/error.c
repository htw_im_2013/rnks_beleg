/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


char *netError[] = {
	"Window is full",
	"Sequence Nr not available",
	"Illegal Ack Number",
	"Illegal Sqn Number",
	"Illegal Request",
	"Server Internal Error",
	"sendto() failed"
};

char *fileError[] = {
	"Unable to open file",
	"Unable to write file"
};
