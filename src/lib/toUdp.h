/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#define DEFAULT_SERVER		"FF11::10"		// loopback interface
#define DEFAULT_FAMILY		AF_INET6		// IPv6
#define DEFAULT_SOCKTYPE	SOCK_DGRAM		// UDP
#define DEFAULT_PORT		"50000"			// test port
#define DEFAULT_WINDOW		"4"
#define UNKNOWN_NAME		"<unknown>"
