/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "data.h"
#include "config.h"
#include "toUdp.h"


void usage(char *progName) {
	int isClient = strcmp("client", progName);

	fprintf(stderr, P_MESSAGE_1);

	if (isClient == 0) {
		fprintf(stderr, P_MESSAGE_6, progName);
	} else {
		fprintf(stderr, P_MESSAGE_11, progName);
	}

	fprintf(stderr, P_MESSAGE_7, (DEFAULT_SERVER == NULL) ? "loopback address" : DEFAULT_SERVER);
	fprintf(stderr, P_MESSAGE_8, DEFAULT_PORT);
	fprintf(stderr, P_MESSAGE_9);

	if (isClient != 0) {
		fprintf(stderr, P_MESSAGE_12);
	}

	exit(1);
}


void exitWithError(char* errorMessage) {
	fprintf(stderr, "--> ERROR\n\t%s\n", errorMessage);
	exit(EXIT_FAILURE);
}


int verifyArguments(int argc, char* argv[], char* server, char* filename, char* port, char* windowSize, int* errorRate) {
	int res = 0;
	int i;

	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (((argv[i][0] == '-') || (argv[i][0] == '/')) && (argv[i][1] != 0) && (argv[i][2] == 0)) {
				switch (tolower(argv[i][1])) {
					case 'a':								// Server Adress
						if (argv[i + 1]) {
							if (argv[i + 1][0] != '-') {
								strcpy(server, argv[++i]);
								break;
							}
						}
						res = 1;
						break;
					case 'p':								// Server Port
						if (argv[i + 1]) {
							if (argv[i + 1][0] != '-') {
								strcpy(port, argv[++i]);
								break;
							}
						}
						res = 1;
						break;
					case 'f':								// File Name
						if (argv[i + 1]) {
							if (argv[i + 1][0] != '-') {
								if (strcpy(filename, argv[++i]) == "") {
									res = 1;
								}
								break;
							}
						}
						res = 1;
						break;
					case 'w':								// Window Size
						if (argv[i + 1]) {
							if (argv[i + 1][0] != '-') {
								strcpy(windowSize, argv[++i]);
								if (atoi(windowSize) > MAX_WINDOW) {
									res = 1;
								}
								break;
							}
						}
						res = 1;
						break;
					case 'e':								// error Rate
						if (argv[i + 1]) {
							if (argv[i + 1][0] != '-') {
								*errorRate = atoi(argv[++i]);
								break;
							}
						}
						res = 1;
						break;
					default:
						res = 1;
						break;
				}
			} else {
				res = 1;
			}
		}
	}

	if (res == 1) {
		usage(argv[0]);
	}

	return res;
}
