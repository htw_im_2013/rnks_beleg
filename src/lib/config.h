/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#define P_MESSAGE_1  "\nHow to use.\n"
#define P_MESSAGE_2  "\n%s [-a adress] [-p port]\n\n"
#define P_MESSAGE_3  " address\tIP address on which to bind. (default: unspecified address)\n"
#define P_MESSAGE_4  " port\t\tPort on which to bind. (default: %s)\n"
#define P_MESSAGE_5  "Hit Ctrl-C to terminate\n"
#define P_MESSAGE_6  "\n%s [-s server] [-p port] [-f file] [-w window_size]\n\n"
#define P_MESSAGE_7  " server\tName or IP address on wich to connect. (default: %s)\n"
#define P_MESSAGE_8  " port\t\tPort on which to connect. (default: %s)\n"
#define P_MESSAGE_9  " file\t\tFile wich to recieve. (default: empty)\n"
#define P_MESSAGE_10 " window_size\t\twindow size of Selective Repeat Protocol (maximum: 10).\n"
#define P_MESSAGE_11  "\n%s [-s server] [-p port] [-f file] [-w window_size] [-e error_type]\n\n"
#define P_MESSAGE_12 " error_type\t\tError to test (Packet loss: 1)\n"

/**
 * print how to use this program
 * @param progName
 */
void usage(char* progName);


/**
 * print error message and exit program
 * @param errorMessage to print to console
 */
void exitWithError(char* errorMessage);


/**
 * verify arguments from given argument list
 * @param  argc       number of arguments
 * @param  argv       arguments
 * @param  server     IP
 * @param  filename   
 * @param  port       
 * @param  windowSize
 * @param  errorRate  
 * @return            0: ok | 1: error
 */
int verifyArguments(int argc, char* argv[], char* server, char* filename, char* port, char* windowSize, int* errorRate);
