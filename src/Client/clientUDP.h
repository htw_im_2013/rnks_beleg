/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


/**
 * initialize client
 * @param multicastIP   IP of multicast group to send data to
 * @param multicastPort port of this multicast group
 */
SOCKET* initClient(char* multicastIP, char* multicastPort);


/**
 * read frame from server
 * @return frame-struct sent from server
 */
struct frame* receiveMFromServer();


/**
 * send frames to either multicast group or duplicates to unicast adress
 * @param  frameOut       frame-struct to send
 * @param  sendingUnicast 0: send to multicast group | 1: send to unicast address
 * @return                NULL if everything is ok, or frame with NACK information from server
 */
struct frame* sendDataToServer(struct frame *frameOut, int sendingUnicast, int maxSeqNr);


/**
 * try to connect to several server
 * @param  frameOut   frame-struct to send to server
 * @param  windowSize from argument list
 * @return            number of servers connected with this client
 */
int sendHello(struct frame* frameOut, char* windowSize);


/**
 * try to disconnect with connected server
 * @param  serverCount number of connected server
 * @return             NULL if all servers got all packages and acknowledged to close
 */
struct frame* sendClose(int* serverCount);


/**
 * close socket connection
 */
void exitClient();
