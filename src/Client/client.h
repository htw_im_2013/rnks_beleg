/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


int main(int argc, char* argv[]);


/**
 * fill frame with data and metadata
 * @param frame       frame structure to fill
 * @param fileContent data to send
 */
void fillFrame(struct frame* frame, struct fileContent* fileContent);


/**
 * get data by specified sequence number (for NACK)
 * @param  content list of data packages
 * @param  senr    sequence number
 * @return         correct data package
 */
struct fileContent* getFileContentByseqNr(struct fileContent* content, unsigned int senr);
