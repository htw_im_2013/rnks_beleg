/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <ctype.h>
#include <tchar.h>
#include <sys/types.h>
#include <windows.h>

#include "../lib/timer.h"
#include "../lib/data.h"
#include "../lib/config.h"


static struct sockaddr_in6 unicastAddr;
ADDRINFO* multicastAddr;
int unicastAddrSize = sizeof(struct sockaddr_in6);
SOCKET cSocket;


void exitClient() {
	closesocket(cSocket);
	if (WSACleanup() == SOCKET_ERROR) {exitWithError("WSACleanup() failed");}
}


SOCKET* initClient(char* multicastIP, char* multicastPort) {
	u_long nonBlocking;
	WSADATA wsaData;
	ADDRINFO hints = {0};
	
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {exitWithError("WSAStartup() failed");}

	/* Resolve destination address for multicast datagrams */
	hints.ai_family   = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags    = AI_NUMERICHOST;
	if ( getaddrinfo(multicastIP, multicastPort, &hints, &multicastAddr) != 0 ) {exitWithError("getaddrinfo() failed");}

	printf("\n\n**** Client started ****\nUsing %s\n", multicastAddr->ai_family == PF_INET6 ? "IPv6" : "IPv4");

	/* Create socket for sending multicast datagrams */
	if ((cSocket = socket(multicastAddr->ai_family, multicastAddr->ai_socktype, 0)) == INVALID_SOCKET) {exitWithError("socket() failed");}

	/* non blocking socket - with timer */
	nonBlocking = 1;
	ioctlsocket(cSocket, FIONBIO, &nonBlocking);

	return &cSocket;
}


struct frame* receiveMFromServer() {
	int receivedBytes;
	struct frame *frameIn = (struct frame*)malloc(sizeof(struct frame));

	receivedBytes = recvfrom(cSocket, (char*)frameIn, sizeof(*frameIn), 0, (struct sockaddr*) &unicastAddr, &unicastAddrSize);

	if (receivedBytes == SOCKET_ERROR) {
		exitClient();
		exitWithError("recvfrom() failed");
	}
	
	if (receivedBytes == 0) {
		exitClient();
		exitWithError("Server closed connection");
	}

	return frameIn;
}


int sendHello(struct frame* frameOut, char* windowSize) {
	int recvcc;
	int i;
	int serverCount = 0;
	struct timeval timer;
	struct frame* frameIn = (struct frame*)malloc(sizeof(struct frame));
	fd_set rfds;

	if (sendto(cSocket, (const char*)frameOut, sizeof(*frameOut), 0, multicastAddr->ai_addr,multicastAddr->ai_addrlen) == SOCKET_ERROR) {
		exitClient();
		exitWithError("sendto() failed");
	}

	printf("try to connect to server...\n");

	for (i = 0; i < TIMEOUT; i++) {

		timer.tv_sec = 0;
		timer.tv_usec = TIMEOUT_INT * 1000;
		FD_ZERO(&rfds);
		FD_SET(cSocket, &rfds);

		if (select(cSocket + 1, &rfds, NULL, NULL, &timer) != 0) {
			recvcc = recvfrom(cSocket, (char*)frameIn, sizeof(*frameIn), 0, (struct sockaddr*) &unicastAddr, &unicastAddrSize);
			
			if (recvcc == SOCKET_ERROR) {
				exitClient();
				exitWithError("recvfrom() failed");
			}

			if (frameIn->type == RES_HELLO) {
				printf("received connection acknowledgement from server %d\n", ++serverCount);
				windowSize = (atoi(windowSize) > frameOut->dataLength) ? itoa(frameIn->dataLength,windowSize,10) : windowSize;
			}
		}
	}

	free(frameIn);
	return serverCount;
}


struct frame* sendClose(int* serverCount) {
	int i;
	int ackCount = 0;
	struct timeval timer;
	fd_set rfds;
	struct frame* frameIn = (struct frame*)malloc(sizeof(struct frame));
	struct frame frameOut;
	
	frameOut.type = REQ_CLOSE;

	if (sendto(cSocket, (const char*)&frameOut, sizeof(frameOut), 0, multicastAddr->ai_addr,multicastAddr->ai_addrlen) == SOCKET_ERROR) {
		exitClient();
		exitWithError("sendto() failed");
	}

	printf("try to close connection...\n");

	for (i = 0; i < TIMEOUT; i++) {
		timer.tv_sec = 0;
		timer.tv_usec = TIMEOUT_INT * 1000;
		FD_ZERO(&rfds);
		FD_SET(cSocket, &rfds);

		if (select(cSocket + 1, &rfds, NULL, NULL, &timer) != 0) {
			frameIn = receiveMFromServer();

			if (frameIn->type == RES_CLOSE) {
				printf("received close acknowledgement from server %d\n", ++ackCount);
				(*serverCount)--;
			} else if (frameIn->type == RES_NACK) {
				return frameIn;
			}
		}
	}

	if ((*serverCount) > 0) {
		printf("%d close %s missing\n", *serverCount, ((*serverCount) > 1) ? "acknowledgements" : "acknowledgement");
	}

	free(frameIn);
	return NULL;
}


struct frame* sendDataToServer(struct frame* frameOut, int sendingUnicast, int maxSeqNr) {
	struct timeval timer;
	fd_set rfds;
	int result;
	timer.tv_sec  = 0;
	timer.tv_usec = TIMEOUT_INT * 1000;
	FD_ZERO(&rfds);
	FD_SET(cSocket, &rfds);
	
	result = (sendingUnicast) ? sendto(cSocket, (const char *)frameOut, sizeof(*frameOut), 0, (struct sockaddr*)&unicastAddr, unicastAddrSize) : sendto(cSocket, (const char *)frameOut, sizeof(*frameOut), 0, multicastAddr->ai_addr,multicastAddr->ai_addrlen);
	
	if (!result){
		exitClient();
		exitWithError("sendto() failed");
		
	}

	printf("package %d sent\n", (frameOut->seqNr % maxSeqNr));
	
	return (select(cSocket + 1, &rfds, NULL, NULL, &timer) != 0) ? receiveMFromServer() : NULL;
}
