/**************************************************************
 *															  *
 * Beleg im Modul Rechnernetze und Kommunikationssysteme 2015 *
 * Autoren: Huy Do Duc, Johannes Metzler, Dennis Wittchen	  *
 * 															  *
 **************************************************************/


#include <dos.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <winsock2.h>

#include "client.h"
#include "clientUDP.h"
#include "../lib/data.h"
#include "../lib/toUdp.h"
#include "../lib/config.h"
#include "../lib/timer.h"
#include "../lib/file.h"


int main(int argc, char* argv[]) {
	struct fileContent* fileContent;
	struct fileContent* fileContentAnchor;
	struct frame* frameIn	= (struct frame*)malloc(sizeof(struct frame));
	struct frame* frameOut 	= (struct frame*)malloc(sizeof(struct frame));
	struct timer* timers 	= NULL;
	
	char filename[100];	
	char server[100]; 	
	char port[10]; 	
	char windowSize[10];
	
	int errorRate		= -1;
	int sendingUnicast	= 0;
	int serverCount		= 0;
	int sqnr_counter 	= 0;
	int currentSeqNr;
	int SeqNrLength;
	int maxSeqNr		= 0;

	/************ INITIALIZE CLIENT ************/
	strcpy(filename, "test.txt");
	strcpy(server, DEFAULT_SERVER);
	strcpy(port, DEFAULT_PORT);	
	strcpy(windowSize, DEFAULT_WINDOW);
	if (verifyArguments(argc, argv, server, filename, port, windowSize, &errorRate) == 0) {
		initClient(server, port);
	} else {
		exitWithError("argument validation failed");
	}
	maxSeqNr = (2*atoi(windowSize))-1;

	/************ BUFFER FILE ************/
	if ((fileContent = loadFile(filename, &SeqNrLength)) == NULL) {
		exitClient();
		exitWithError("loading file content failed");
	}
	fileContentAnchor = fileContent;
	
	/************ HELLO ************/
	strcpy(frameOut->data, filename);
	frameOut->type 			= REQ_HELLO;
	frameOut->dataLength 	= atoi(windowSize);	
	frameOut->seqNr 		= SeqNrLength;
	
	if ((serverCount = sendHello(frameOut, windowSize)) < 1) {
		exitClient();
		exitWithError("no server connected");
	} else {
		printf("connected to %d server\n", serverCount);
		printf("using window size of %c\n", windowSize);
	}

	/************ DATA ************/
	frameOut->type = REQ_DATA;
	fillFrame(frameOut, fileContent);
	printf("sending %s\n", filename);

	while (1) {
		if (timers != NULL) {
			timers = decrement_timers(timers);
		}

		currentSeqNr 	= frameOut->seqNr;
		frameIn 		= sendDataToServer(frameOut, sendingUnicast, maxSeqNr);
		timers 		 	= add_timer(timers, currentSeqNr);
		
		if (frameIn != NULL) {
			/************ NACK ************/
			printf("NACK seqNr: %d\n", (frameIn->seqNr % maxSeqNr));
			del_timer(timers, frameIn->seqNr);
			fillFrame(frameOut, getFileContentByseqNr(fileContentAnchor, frameIn->seqNr));
			sendingUnicast = 1;
		} else {
			if (fileContent->next != NULL) {
				/************ DATA ************/
				fileContent = fileContent->next;
				frameOut 	= (struct frame*)malloc(sizeof(struct frame));
				fillFrame(frameOut, fileContent);
				sendingUnicast = 0;
			} else {
				/************ CLOSE ************/
				frameIn = sendClose(&serverCount);
				if (frameIn != NULL) {
					fillFrame(frameOut, getFileContentByseqNr(fileContentAnchor, frameIn->seqNr));	
				} else {
					free(frameIn);
					free(frameOut);
					exitClient();
					break;
				}
			}
		}
	}

	return 0;
}


void fillFrame(struct frame* frame, struct fileContent* fileContent){
	strcpy(frame->data, fileContent->data);
	frame->type 		= REQ_DATA;
	frame->dataLength	= fileContent->length;
	frame->seqNr 		= fileContent->seqNr;
}


struct fileContent* getFileContentByseqNr(struct fileContent* content, unsigned int senr) {
	while (1) {
		if (content->seqNr == senr) {return content;}
		if ((content = content->next) == NULL) {return NULL;}
	}
}
