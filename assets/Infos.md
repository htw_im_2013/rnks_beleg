# Rechnernetze / Kommunikationssysteme

## Beleg

+ Protokoll für zuverlässiges Multicast
+ 3 Teilnehmer
+ IPv6
FF11::10 (Node local) loopback!
FF12::10 (link local)
TTL = 1
+ selective repeat (NACK)(fensterbasiert) - negative Quittung

### Sender
+ Intervall 300ms
+ senden der Anfrage und Empfang der Unicast-Adressen
+ nach 3 Intervallen müssen alle Bestätigungen da sein
+ Fenstergröße = 4 (4 Pakete ohne Quittung können gesendet werden) [0 1 2 3]
++ nach 3 * Intervall keine Quittung —> Fenster verschieben [1 2 3 4]

### Fehler
+ Empfänger 1 Fehler für jedes 3. Paket
+ sendet NACK = 1 (Paketnummer)
+ Sender schickt Paket nochmal (unicast)


### Rahmenbedingungen
+ C-Konsolenprogramm
+ lauffähig auf Windows 8
+ Erläuterung zum Aufruf von Client und Server
+ Selbstständigkeitserklärung (Quellenangaben)

### Aspekte
+ Textdatei **zeilenweise** via UDPv6 / IPv6 übertragen
+ Parameterübergabe an Server und Client
++ Multicast-Adresse
++ Fenstergröße
++ Port
++ Filename
++ Fehlerqoute
+ ARQ-Mechanismus (automatic repeat request) [wikipedia](https://de.wikipedia.org/wiki/ARQ-Protokoll)
+ Multicast-Empfänger Sequenznummern in Paketen prüfen (ggf. NACK via unicast senden)


### Multicast
+ zum Test mit mehreren Client ist eigentlich verboten, einen Port mehrfach zu nutzen. `reuse`

***

## Codeschnipsel von S. Kühn

### client.c
		client -a 127.0.0.1 -p 8888 -f text.txt -w 4
+ Reihenfolge der Parameter ist nicht relevant
	
		Usage(char* ProgName) —> void
+ gibt Hinweise zur Verwendung/Aufruf der Software und beendet den Client

		printAnswer(struct answer* answPtr) —> int
+ Ausgabe auf Konsole

		main(int argc, char* argv[]) —> int

***

### config.h
+ „man-pages“

***

### data.h
		extern char* errorTable[]
+ diverse Fehlermeldungen
+ referenziert Deklaration aus `error.c`

		struct request {
			unsigned char 	ReqType;
			long FlNr;	
			unsigned long SeNr;
			char name[256];
		};
+ `ReqType` kann H (Hello),D (Data), C (Close) sein

		struct answer {
			unsigned char AnswType;
			unsigned FlNr;	
			unsigned SeNo;
		};
+ `AnswType` kann H (Hello), O (OK), N (NACK), C (Close) sein

***

### toUdp.h
+ Standardkonfiguration